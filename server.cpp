#include <errno.h>
#include<iostream>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <vector>
#include <unistd.h>

#define BACKLOG 10 
#define BUFFER_SIZE 4096

int main(int argc, char *argv[]) {
    int status;
    struct addrinfo hints;
    struct addrinfo *servinfo;      
    const char * port = argv[1];    
    int sockfd, conn_fd;
    struct sockaddr_storage conn_addr; 
    socklen_t addr_size; 
    addr_size = sizeof conn_addr;
    
    if (argc != 2) {
	    fprintf(stderr,"port number error\n");
	    errno = ENOENT;
	    return -1;
	}
    
    memset(&hints, 0, sizeof hints); 
    hints.ai_family = AF_INET;       
    hints.ai_socktype = SOCK_STREAM; 
    hints.ai_flags = AI_PASSIVE;     
    
    if ((status = getaddrinfo(NULL, port, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        return -1;
    }   
    
    sockfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if (sockfd == -1) {
        fprintf(stderr, "sockfd error: %s\n", strerror(errno));
        freeaddrinfo(servinfo); 
        return -1;
    }
    
    if (bind(sockfd, servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
        fprintf(stderr, "bind error: %s\n", strerror(errno));
        freeaddrinfo(servinfo); 
        return -1;
    }
    if (listen(sockfd, BACKLOG) == -1) {
        fprintf(stderr, "listen error: %s\n", strerror(errno));
        freeaddrinfo(servinfo); 
        return -1;
    }
    for(;;) { 
        conn_fd = accept(sockfd, (struct sockaddr *)&conn_addr, &addr_size);
        if (conn_fd == -1) {
            fprintf(stderr, "accept error: %s\n", strerror(errno));
            freeaddrinfo(servinfo); 
            return -1;
        }
         
        std::vector<char> buffer;
        unsigned char buf[BUFFER_SIZE];
        int bytes_recv;
        do {
            bytes_recv = recv(conn_fd, reinterpret_cast<void*>(&buf), BUFFER_SIZE, 0);
            if (bytes_recv == -1) {  
                fprintf(stderr, "receive error: %s\n", strerror(errno));
                freeaddrinfo(servinfo); 
                return -1;
            }
            buffer.insert(buffer.end(), buf, buf + bytes_recv);
            memset(buf, 0, BUFFER_SIZE); 
        } while (bytes_recv > 0);
            
        char * data = buffer.data();
        int total_bytes_recv = buffer.size();
        int bytes_to_send = buffer.size();
        int bytes_sent;
        int total_bytes_sent = 0;
        do {
            if (bytes_to_send <= 0) {
                break;
            }
            bytes_sent = send(conn_fd, data, bytes_to_send, 0);
            if (bytes_sent == -1) {  
                fprintf(stderr, "send error: %s\n", strerror(errno));
                freeaddrinfo(servinfo); 
                return -1;
            } 
            if (bytes_sent == 0) {  
                fprintf(stderr, "error: connection closed\n");
                errno = EAGAIN;
                freeaddrinfo(servinfo); 
                return -1;
            }
            total_bytes_sent += bytes_sent;
            data += bytes_sent;
            bytes_to_send -= bytes_sent;
            bytes_sent = send(conn_fd, data, bytes_to_send, 0);         
        } while (bytes_sent > 0);
        
        if (total_bytes_sent != total_bytes_recv) {  
            fprintf(stderr, "send error: %d bytes missed\n", total_bytes_recv - bytes_sent);
            errno = EAGAIN;
            freeaddrinfo(servinfo); 
            return -1;
        }
    }
    freeaddrinfo(servinfo); 
    return 0;
}
