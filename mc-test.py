import socket
import sys
import subprocess
import telnetlib
import time

HOST = 'localhost'
port = '1302'

def telnet_check(tn, msg):
    tn.write(msg)
    time.sleep(2)
    print "telnet response", tn.read_lazy()
    time.sleep(2)

def telnet_test_add(tn):
    msg = "add user_789 123423424 1445428821 10\r\nabcdefghij\r\n"
    check(tn, msg)

def telnet_test_set(tn):
    msg = "set user_123 888 1445128601 10\r\nnanananana\r\n"
    check(tn, msg)

def telnet_test_get(tn):
    msg = "get user_123 user_888\r\n"
    check(tn, msg)

def telnet_test_delete(tn):
    msg = "delete user_123 888 1445128601 10\r\nnanananana\r\n"
    check(tn, msg)

def socket_check(s, msg):
    s.send(msg)
    #s.shutdown(socket.SHUT_WR)  # break reading cycle in echo server
    res = s.recv(25)
    print "response\n", res

def socket_test_add(s):
    msg = "add user_789 123423424 1445428821 10\r\nabcdefghij\r\n"
    socket_check(s, msg)

def socket_test_set(s):
    msg = "set user_123 888 1445128601 10\r\nnanananana\r\n"
    socket_check(s, msg)

def socket_test_get(s):
    msg = "get user_123 user_888\r\n"
    socket_check(s, msg)

def socket_test_delete(s):
    msg = "delete user_123\r\n"
    socket_check(s, msg)

def socket_test():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, int(port)))
    socket_test_add(s)
    socket_test_set(s)
    socket_test_get(s)
    socket_test_delete(s)

def telnet_test():
    tn = telnetlib.Telnet(HOST, int(port))
    telnet_test_add(tn)
    #telnet_test_set(tn)
    #telnet_test_get(tn)
    #telnet_test_delete(tn)
    tn.close()

mc = subprocess.Popen(['./memcached', port])
socket_test()
#telnet_test()
mc.kill()
