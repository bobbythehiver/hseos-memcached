#pragma once

#include <string>
#include <vector>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

class RBuffer {
public:
    explicit RBuffer(size_t buffer_size)
        : buffer_(buffer_size)
        , pos_(0)
        , end_(buffer_size)
    {}
    virtual ~RBuffer() {}

    char ReadChar();
    void ReadCharCheck(char check);
    uint32_t ReadUint32();

    std::string ReadField(char sep);  
    std::string ReadField(std::string sep);
    std::vector<char> ReadBytes(size_t bytes_num);

protected:
    virtual void ReadMore() = 0;

    std::vector<char> buffer_;
    size_t pos_;
    size_t end_;
};


class WBuffer {
public:
    explicit WBuffer(size_t buffer_size)  
        : buffer_(buffer_size)
        , pos_(0)
    {}
    virtual ~WBuffer() {}
    
    size_t GetSize();
    
    void WriteChar(char); 
    void WriteUint32(uint32_t);  

    void WriteField(std::string field, char sep); 
    void WriteField(std::string field);  
    void WriteBytes(const std::vector<char>& buffer); 

    virtual void Flush() = 0; 

protected:
    std::vector<char> buffer_;
    size_t pos_;
};


class SocketRBuffer : public RBuffer {
public:
    SocketRBuffer(size_t buf_size, int fd);
    void Resize(size_t new_buf_size);

protected:
    virtual void ReadMore() override;

private:
    int fd_;
    int size_;
};

class SocketWBuffer : public WBuffer {
public:
    SocketWBuffer(size_t buf_size, int fd) : WBuffer(buf_size), fd_(fd) {}

    virtual void Flush() override;

private:
    int fd_;
};

