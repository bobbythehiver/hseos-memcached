#pragma once

#include <algorithm>
#include <iterator>
#include <limits.h>
#include <queue>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <unordered_map>
#include <vector>

template<typename Value, typename Time>
struct Item {
    Value value_;
    Time delete_time_;
    Time update_time_;
    
    Item(Value value, Time exp_time) {}

    Item() {}

    bool isvalid() {
        return true;
    }
};

template<>
struct Item<std::vector<char>, int32_t> {
    std::vector<char> value_;
    int32_t delete_time_ = INT_MAX;
    int32_t update_time_;
    
    Item(std::vector<char> value, int32_t exp_time) 
        : value_(value) {
        time_t td;
        td=time(NULL);
        update_time_ = (int32_t)td;
        if (update_time_ + exp_time > update_time_) {
            delete_time_ = update_time_ + exp_time;
        }
    }

    Item() : value_(std::vector<char>()) {
        time_t td;
        td=time(NULL);
        update_time_ = (int32_t)td;
    }
    
    bool isvalid() {
        time_t td;
        td=time(NULL);
        return delete_time_ >= (int32_t)td;
    }

    void update_time() {
        time_t td;
        td=time(NULL);
        update_time_ = (int32_t)(td);
    }
};

template<typename Key, typename Value, typename Time>
class Cache {
public:
    explicit Cache(size_t size) : size_(size) {
    } 
     
    ~Cache();
    
    bool set(Key k, Value& v, Time t);
    bool get(Key k, Value& v);
    bool remove(Key k);
    bool add(Key k, Value& v, Time t);
    bool replace(Key k, Value& v, Time t);
    bool append(Key k, Value& v);
    bool prepend(Key k, Value& v);
    void remove_overdue();
    void remove_earliest();
    bool isfull();
    bool isempty();
    bool get_update_time(Key k, Time& t);
    bool exist(Key k);
    
private:
    size_t size_ = 0;
    std::unordered_map<Key, Item<Value, Time>> items_;
    std::queue<std::pair<Key, Time> > update_;
};

template<typename Key, typename Value, typename Time> 
bool Cache<Key, Value, Time>::exist(Key k) {
    typename std::unordered_map<Key, Item<Value, Time>>::iterator iter = items_.find(k);
    if (iter == items_.end()) {
        return false;
    }
    return true;
}

template<typename Key, typename Value, typename Time> 
bool Cache<Key, Value, Time>::get_update_time(Key k, Time& t) {
    if (!exist(k)) {
        return false;
    }
    t = items_[k].update_time_;
    return true;
}

template<typename Key, typename Value, typename Time> 
bool Cache<Key, Value, Time>::isempty() {
    return items_.empty();
}

template<typename Key, typename Value, typename Time> 
bool Cache<Key, Value, Time>::isfull() {
    return items_.size() == size_;
}

template <typename Key, typename Value, typename Time>
bool Cache<Key, Value, Time>::remove(Key k) {
    if (!exist(k)) {
        return false;
    }
    items_.erase(k);
    return true;
}

template<typename Key, typename Value, typename Time> 
void Cache<Key, Value, Time>::remove_overdue() {}

template<typename Key, typename Value, typename Time> 
void Cache<Key, Value, Time>::remove_earliest() {
    while (1) {
        std::pair<Key, Time> p = update_.front();
        update_.pop();
        Key k = p.first;
        Time t = p.second;
        if (exist(k)) {
            if (items_[k].update_time_ == t) {
                remove(k);
                break;
            }
        }
    }
    return;
}

template<typename Key, typename Value, typename Time> 
bool Cache<Key, Value, Time>::get(Key k, Value& v) {
    if (!exist(k)) {
        return false;
    }
    if (!items_[k].isvalid())  {
        return false;
    }
    items_[k].update_time();
    update_.push(std::make_pair(k, items_[k].update_time_));
    v = items_[k].value_;
    return true; 
}

template <typename Key, typename Value, typename Time>
bool Cache<Key, Value, Time>::set(Key k, Value& v, Time t) {
    if (!exist(k)) {
        if (isfull()) {
            remove_earliest();
        }
        Item<Value, Time>* new_item = new Item<Value, Time>(v, t);
        items_[k] = *new_item;
    } else { 
        items_[k] = Item<Value,Time>(v, t);
    }
    items_[k].update_time();
    update_.push(std::make_pair(k, items_[k].update_time_));
    return true;
}

template <typename Key, typename Value, typename Time>
bool Cache<Key, Value, Time>::add(Key k, Value& v, Time t) {
    if (exist(k)) {
        return false;
    }
    if (isfull()) {
        remove_earliest();
    }
    Item<Value, Time>* new_item = new Item<Value, Time>(v, t);
    items_[k] = *new_item;
    items_[k].update_time();
    update_.push(std::make_pair(k, items_[k].update_time_));
    return true;
}

template <typename Key, typename Value, typename Time>
bool Cache<Key, Value, Time>::replace(Key k, Value& v, Time t) {
    if (!exist(k)) {
        return false;
    }
    items_[k] = Item<Value,Time>(v, t);
    items_[k].update_time();
    update_.push(std::make_pair(k, items_[k].update_time_));
    return true;
}

template <typename Value>
struct Insert {
    void front(Value& old_value, Value& new_value) {
        return;
    }
    void back(Value& old_value, Value& new_value) {
        return;
    }
};

template <>
struct Insert<std::vector<char> > {
    void front(std::vector<char>& old_value, std::vector<char>& new_value) {
        std::vector<char> value = new_value;
        int new_size = value.size();
        value.resize(old_value.size() + value.size());
        std::copy(old_value.begin(), old_value.end(), value.begin() + new_size);
        old_value = value;
        return;
    }
    void back(std::vector<char>& old_value, std::vector<char>& new_value) {
        int old_size = old_value.size() - sizeof(int);
        old_value.resize(old_value.size() + new_value.size());
        std::copy(old_value.begin() + old_size, 
                    old_value.begin() + old_size + sizeof(int), 
                    old_value.end() - sizeof(int));
        std::copy(new_value.begin(), new_value.end(), old_value.begin() + old_size);
        return;
    }
};

template <typename Key, typename Value, typename Time>
bool Cache<Key, Value, Time>::append(Key k, Value& v) {
    if (!exist(k)) {
        return false;
    }
    Value value = items_[k].value_;
    Insert<Value> insrt;
    insrt.back(value, v);
    items_[k].value_ = value;
    items_[k].update_time();
    update_.push(std::make_pair(k, items_[k].update_time_));
    return true;
}

template <typename Key, typename Value, typename Time>
bool Cache<Key, Value, Time>::prepend(Key k, Value& v) {
    if (!exist(k)) {
        return false;
    }
    Value value = items_[k].value_;
    Insert<Value> insrt;
    insrt.front(value, v);
    items_[k].value_ = value;
    items_[k].update_time();
    update_.push(std::make_pair(k, items_[k].update_time_));
    return true;
}

template <typename Key, typename Value, typename Time>
Cache<Key, Value, Time>::~Cache() {}
