#include "gtest/gtest.h"
#include "buffer.h"

#define BUFFER_SIZE 4096

TEST(SocketRBuffer, ReadChar) {
    int pipefd[2];
    pipe(pipefd);
    
    //fprintf(stderr, "read end %d write end %d\n", pipefd[0], pipefd[1]);
    char * str = "str";
    int bytes_to_write = strlen(str);
    int bytes_written;
    int total_bytes_written = 0;
    do {
        bytes_written = write(pipefd[1], reinterpret_cast<void*>(str), bytes_to_write);
        total_bytes_written += bytes_written;
        bytes_to_write -= bytes_written;
        str += bytes_written;
        //fprintf(stderr, "cur %d total %d left %d\n", bytes_written, total_bytes_written, bytes_to_write);
    } while (bytes_to_write > 0);
    close(pipefd[1]);
    SocketRBuffer rbuf(2, pipefd[0]);

    EXPECT_EQ('s', rbuf.ReadChar());
    EXPECT_EQ('t', rbuf.ReadChar());
    EXPECT_EQ('r', rbuf.ReadChar());
    close(pipefd[0]);
}

TEST(SocketRBuffer, ReadUint32) {
    int pipefd[2];
    pipe(pipefd);
    
    char * str = "14 847385638,845 ";
    int bytes_to_write = strlen(str);
    int bytes_written;
    int total_bytes_written = 0;
    do {
        bytes_written = write(pipefd[1], reinterpret_cast<void*>(str), bytes_to_write);
        total_bytes_written += bytes_written;
        bytes_to_write -= bytes_written;
        str += bytes_written;
    } while (bytes_to_write > 0);
    close(pipefd[1]);
    SocketRBuffer rbuf(2, pipefd[0]);

    EXPECT_EQ(14, rbuf.ReadUint32());
    EXPECT_EQ(' ', rbuf.ReadChar());
    EXPECT_EQ(847385638, rbuf.ReadUint32());
    EXPECT_EQ(',', rbuf.ReadChar());
    EXPECT_EQ(845, rbuf.ReadUint32());
    close(pipefd[0]);
}

TEST(SocketRBuffer, ReadField) {
    int pipefd[2];
    pipe(pipefd);
 
    char * str = "alpha beta! gamma,delta.";
    int bytes_to_write = strlen(str);
    int bytes_written;
    int total_bytes_written = 0;
    do {
        bytes_written = write(pipefd[1], reinterpret_cast<void*>(str), bytes_to_write);
        total_bytes_written += bytes_written;
        bytes_to_write -= bytes_written;
        str += bytes_written;
    } while (bytes_to_write > 0);
    close(pipefd[1]);
    SocketRBuffer rbuf(2, pipefd[0]);

    EXPECT_EQ("alpha beta", rbuf.ReadField('!'));
    EXPECT_EQ('!', rbuf.ReadChar());
    EXPECT_EQ(" gamma", rbuf.ReadField(','));
    EXPECT_EQ(',', rbuf.ReadChar());
    EXPECT_EQ("delta", rbuf.ReadField('.'));
    close(pipefd[0]);
}

TEST(SocketRBuffer, Empty) {
    int pipefd[2];
    pipe(pipefd);
    
    close(pipefd[1]);
    EXPECT_THROW(SocketRBuffer(2, pipefd[0]), std::runtime_error);
    
    close(pipefd[0]);
}

TEST(SocketRBuffer, Closed) {
    int pipefd[2];
    pipe(pipefd);
   
    close(pipefd[0]);
    close(pipefd[1]);
    EXPECT_THROW(SocketRBuffer(2, pipefd[0]), std::runtime_error);  
}

TEST(SocketWBuffer, WriteChar) {
    int pipefd[2];
    pipe(pipefd);
    
    SocketWBuffer wbuf(2, pipefd[1]);
    wbuf.WriteChar('s');
    wbuf.WriteChar('t');
    wbuf.WriteChar('r');
    wbuf.Flush();
    close(pipefd[1]);
    
    std::string buffer = "";
    unsigned char buf[BUFFER_SIZE];
    int bytes_read;
    do {
        bytes_read = read(pipefd[0], reinterpret_cast<void*>(&buf), sizeof(buf));
        if (bytes_read == -1) {
            throw std::runtime_error("Failed to read from socket in SocketRBuffer.ReadMore");
        }
        buffer.append(buf, buf + bytes_read);     
    } while (bytes_read > 0);
    
    EXPECT_EQ("str", buffer);
    close(pipefd[0]);
}

TEST(SocketWBuffer, WriteUint32) {
    int pipefd[2];
    pipe(pipefd);
    
    SocketWBuffer wbuf(2, pipefd[1]);
    wbuf.WriteUint32(29);
    wbuf.WriteUint32(4651);
    wbuf.WriteChar(' ');
    wbuf.WriteUint32(64298872);
    wbuf.Flush();
    close(pipefd[1]);
    
    std::string buffer = "";
    unsigned char buf[BUFFER_SIZE];
    int bytes_read;
    do {
        bytes_read = read(pipefd[0], reinterpret_cast<void*>(&buf), sizeof(buf));
        if (bytes_read == -1) {
            throw std::runtime_error("Failed to read from socket in SocketRBuffer.ReadMore");
        }
        buffer.append(buf, buf + bytes_read);     
    } while (bytes_read > 0);
    
    EXPECT_EQ("294651 64298872", buffer);
    close(pipefd[0]);
}

TEST(SocketWBuffer, WriteBytes) {
    int pipefd[2];
    pipe(pipefd);
    
    SocketWBuffer wbuf(2, pipefd[1]);
    wbuf.WriteField("alpha", ' ');
    wbuf.WriteField("beta");
    wbuf.WriteChar('!');
    wbuf.WriteField("gamma", ',');
    wbuf.WriteChar(' ');
    wbuf.WriteField("delta");
    wbuf.Flush();
    close(pipefd[1]);
    
    std::string buffer = "";
    unsigned char buf[BUFFER_SIZE];
    int bytes_read;
    do {
        bytes_read = read(pipefd[0], reinterpret_cast<void*>(&buf), sizeof(buf));
        if (bytes_read == -1) {
            throw std::runtime_error("Failed to read from socket in SocketRBuffer.ReadMore");
        }
        buffer.append(buf, buf + bytes_read);     
    } while (bytes_read > 0);
    
    EXPECT_EQ("alpha beta!gamma, delta", buffer);
    close(pipefd[0]);
}
TEST(SocketWBuffer, WriteField) {
    int pipefd[2];
    pipe(pipefd);
    
    SocketWBuffer wbuf(2, pipefd[1]);
    wbuf.WriteBytes({'\x62', '\x61', '\x6e', '\x61', '\x6e', '\x61'});
    wbuf.Flush();
    close(pipefd[1]);
    
    std::string buffer = "";
    unsigned char buf[BUFFER_SIZE];
    int bytes_read;
    do {
        bytes_read = read(pipefd[0], reinterpret_cast<void*>(&buf), sizeof(buf));
        if (bytes_read == -1) {
            throw std::runtime_error("Failed to read from socket in SocketRBuffer.ReadMore");
        }
        buffer.append(buf, buf + bytes_read);     
    } while (bytes_read > 0);
    
    EXPECT_EQ("banana", buffer);
    close(pipefd[0]);
}
