#include "gtest/gtest.h"
#include "cache.h"

#include <algorithm>
#include <fstream>
#include <iterator>
#include <stdio.h>
#include <time.h>

#define CACHE_SIZE 4096
#define ITERS 10000000

std::ofstream fout("currstat.txt");

std::vector<char> value = {'v', 'a', 'l'};
std::vector<char> add_value = {'a', 'd', 'd'};
std::vector<char> replace_value = {'r', 'e', 'p', ' ', ' ', ' ', ' '};
std::vector<char> to_append = {'a', 'p', 'p'};
std::vector<char> append_value = {'r', 'e', 'p', 'a', 'p', 'p', ' ', ' ', ' ', ' '};
std::vector<char> to_prepend = {'p', 'r', 'e'};
std::vector<char> prepend_value = {'p', 'r', 'e', 'r', 'e', 'p', 'a', 'p', 'p', ' ', ' ', ' ', ' '};

Cache<std::string, std::vector<char>, int32_t> cache(CACHE_SIZE);

std::string int2str(unsigned n) {
    std::string res = "";
    while (n > 0) {
        res = char(n%10 + '0') + res;
        n /= 10;
    }
    return res;
}

std::string vec2str(std::vector<char> &v) {
    std::string s = "";
    for (size_t i = 0; i < v.size(); ++i) {
        s += v[i];
    }
    return s;
}

void findabsent() {
    for (int i = 1; i <= ITERS; ++i) {
        std::vector<char> return_value;
        std::string key = "user" + int2str(i);
        if (!cache.get(key, return_value)) {
            fprintf(stderr, "%d\n", i);
        }
    }
}

TEST(Cache, Set) {
    clock_t t;
    t = clock();
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        //fprintf(stderr, "key %s\n", key.c_str());
        bool flag = cache.set(key, value, 0);
        EXPECT_EQ(true, flag);
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fprintf(stderr, "clock ticks per operation %lf\n", clocks_per_operation);
    fprintf(stderr, "operation time %lf\n", operation_time);
    fprintf(stderr, "operations per second %lf\n\n", operations_per_sec);
}

TEST(Cache, Get) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        bool flag = cache.get(key, return_value);
         if (ITERS - i < CACHE_SIZE) {
            EXPECT_EQ(value, return_value);
        } else {
            EXPECT_EQ(false, flag);
        }
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fprintf(stderr, "clock ticks per operation %lf\n", clocks_per_operation);
    fprintf(stderr, "operation time %lf\n", operation_time);
    fprintf(stderr, "operations per second %lf\n\n", operations_per_sec);
}

TEST(Cache, Add) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = ITERS; i >= 1; --i) {
        std::string key = "user" + int2str(i);
        bool flag = cache.add(key, add_value, 0);
        if (ITERS - i < CACHE_SIZE) {
            EXPECT_EQ(false, flag);
        } else {
            EXPECT_EQ(true, flag);
        }
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fprintf(stderr, "clock ticks per operation %lf\n", clocks_per_operation);
    fprintf(stderr, "operation time %lf\n", operation_time);
    fprintf(stderr, "operations per second %lf\n\n", operations_per_sec);
}

TEST(Cache, Replace) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        bool flag = cache.replace(key, replace_value, 0);
        if (ITERS <= 2*CACHE_SIZE) {
            if (i <= ITERS - CACHE_SIZE) {
                EXPECT_EQ(true, flag);
                cache.get(key, return_value);
                EXPECT_EQ(replace_value, return_value);
            } else if (i > ITERS - CACHE_SIZE && i <= 2*(ITERS - CACHE_SIZE)) {
                EXPECT_EQ(false, flag);
            } else {
                EXPECT_EQ(true, flag);
                cache.get(key, return_value);
                EXPECT_EQ(replace_value, return_value);
            }
        } else {
            if (i <= CACHE_SIZE) {
                EXPECT_EQ(true, flag);
                cache.get(key, return_value);
                EXPECT_EQ(replace_value, return_value);
            } else {
                EXPECT_EQ(false, flag);
            }
        }
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fprintf(stderr, "clock ticks per operation %lf\n", clocks_per_operation);
    fprintf(stderr, "operation time %lf\n", operation_time);
    fprintf(stderr, "operations per second %lf\n\n", operations_per_sec);
}

TEST(Cache, Append) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        bool flag = cache.append(key, to_append);
        if (ITERS <= 2*CACHE_SIZE) {
            if (i <= ITERS - CACHE_SIZE) {
                EXPECT_EQ(true, flag);
                cache.get(key, return_value);
                EXPECT_EQ(append_value, return_value);
            } else if (i > ITERS - CACHE_SIZE && i <= 2*(ITERS - CACHE_SIZE)) {
                EXPECT_EQ(false, flag);
            } else {
                EXPECT_EQ(true, flag);
                cache.get(key, return_value);
                EXPECT_EQ(append_value, return_value);
            }
        } else {
            if (i <= CACHE_SIZE) {
                EXPECT_EQ(true, flag);
                cache.get(key, return_value);
                EXPECT_EQ(append_value, return_value);
            } else {
                EXPECT_EQ(false, flag);
            }
        }
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fprintf(stderr, "clock ticks per operation %lf\n", clocks_per_operation);
    fprintf(stderr, "operation time %lf\n", operation_time);
    fprintf(stderr, "operations per second %lf\n\n", operations_per_sec);
}

TEST(Cache, Prepend) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        bool flag = cache.prepend(key, to_prepend);
        if (ITERS <= 2*CACHE_SIZE) {
            if (i <= ITERS - CACHE_SIZE) {
                EXPECT_EQ(true, flag);
                cache.get(key, return_value);
                EXPECT_EQ(prepend_value, return_value);
            } else if (i > ITERS - CACHE_SIZE && i <= 2*(ITERS - CACHE_SIZE)) {
                EXPECT_EQ(false, flag);
            } else {
                EXPECT_EQ(true, flag);
                cache.get(key, return_value);
                EXPECT_EQ(prepend_value, return_value);
            }
        } else {
            if (i <= CACHE_SIZE) {
                EXPECT_EQ(true, flag);
                cache.get(key, return_value);
                EXPECT_EQ(prepend_value, return_value);
            } else {
                EXPECT_EQ(false, flag);
            }
        }
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fprintf(stderr, "clock ticks per operation %lf\n", clocks_per_operation);
    fprintf(stderr, "operation time %lf\n", operation_time);
    fprintf(stderr, "operations per second %lf\n\n", operations_per_sec);
}

TEST(Cache, Delete) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        bool flag = cache.remove(key);
        if (ITERS <= 2*CACHE_SIZE) {
            if (i <= ITERS - CACHE_SIZE) {
                EXPECT_EQ(true, flag);
                cache.remove(key);
                flag = cache.get(key, return_value);
                EXPECT_EQ(false, flag);
            } else if (i > ITERS - CACHE_SIZE && i <= 2*(ITERS - CACHE_SIZE)) {
                EXPECT_EQ(false, flag);
            } else {
                EXPECT_EQ(true, flag);
            }
        } else {
            if (i <= CACHE_SIZE) {
                EXPECT_EQ(true, flag);
                cache.remove(key);
                flag = cache.get(key, return_value);
                EXPECT_EQ(false, flag);
            } else {
                EXPECT_EQ(false, flag);
            }
        }
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fprintf(stderr, "clock ticks per operation %lf\n", clocks_per_operation);
    fprintf(stderr, "operation time %lf\n", operation_time);
    fprintf(stderr, "operations per second %lf\n\n", operations_per_sec);
}

TEST(Cache, SetSpeedTest) {
    cache = Cache<std::string, std::vector<char>, int32_t>(CACHE_SIZE);
    clock_t t;
    t = clock();
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        cache.set(key, value, 0);
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fout << ITERS << '\n';
    fout << "SET\n";
    fout << "clock ticks per operation " << clocks_per_operation << '\n';
    fout << "operation time " << operation_time << '\n';
    fout << "operations per second " << operations_per_sec << "\n\n";
}

TEST(Cache, GetSpeedTest) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        cache.get(key, return_value);
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fout << "GET\n";
    fout << "clock ticks per operation " << clocks_per_operation << '\n';
    fout << "operation time " << operation_time << '\n';
    fout << "operations per second " << operations_per_sec << "\n\n";
}

TEST(Cache, AddSpeedTest) {
    cache = Cache<std::string, std::vector<char>, int32_t>(CACHE_SIZE);
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = ITERS; i >= 1; --i) {
        std::string key = "user" + int2str(i);
        cache.add(key, add_value, 0);
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fout << "ADD\n";
    fout << "clock ticks per operation " << clocks_per_operation << '\n';
    fout << "operation time " << operation_time << '\n';
    fout << "operations per second " << operations_per_sec << "\n\n";
}


TEST(Cache, ReplaceSpeedTest) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        cache.replace(key, replace_value, 0);
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fout << "REPLACE\n";
    fout << "clock ticks per operation " << clocks_per_operation << '\n';
    fout << "operation time " << operation_time << '\n';
    fout << "operations per second " << operations_per_sec << "\n\n";
}

TEST(Cache, AppendSpeedTest) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        cache.append(key, to_append);
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fout << ITERS << '\n';
    fout << "APPEND\n";
    fout << "clock ticks per operation " << clocks_per_operation << '\n';
    fout << "operation time " << operation_time << '\n';
    fout << "operations per second " << operations_per_sec << "\n\n";
}

TEST(Cache, PrependSpeedTest) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        cache.prepend(key, to_prepend);
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fout << "PREPEND\n";
    fout << "clock ticks per operation " << clocks_per_operation << '\n';
    fout << "operation time " << operation_time << '\n';
    fout << "operations per second " << operations_per_sec << "\n\n";
}

/*TEST(Cache, DeleteSpeedTest) {
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (size_t i = 1; i <= ITERS; ++i) {
        std::string key = "user" + int2str(i);
        cache.remove(key);
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fout << "DELETE\n";
    fout << "clock ticks per operation " << clocks_per_operation << '\n';
    fout << "operation time " << operation_time << '\n';
    fout << "operations per second " << operations_per_sec << "\n\n";
}
*/

TEST(Cache, DeleteRandSpeedTest) {
    std::vector<int> order(ITERS);
    for (size_t i = 1; i <= ITERS; ++i) {
        order[i] = i;
    }
    std::random_shuffle(order.begin(), order.end());
    clock_t t;
    t = clock();
    std::vector<char> return_value;
    for (auto i : order) {
        std::string key = "user" + int2str(i);
        cache.remove(key);
    }
    t = clock() - t;
    double clocks_per_operation = (double)(t) / ITERS;
    double operations_per_sec = CLOCKS_PER_SEC / clocks_per_operation;
    double operation_time = clocks_per_operation / CLOCKS_PER_SEC;
    fout << "DELETE_RANDOM\n";
    fout << "clock ticks per operation " << clocks_per_operation << '\n';
    fout << "operation time " << operation_time << '\n';
    fout << "operations per second " << operations_per_sec << "\n\n";
}