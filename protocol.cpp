#include "protocol.h"
#include "cache.h"

#include <mutex>
#include <stdexcept>
#include <thread>

//std::mutex mtx;

MC_COMMAND CommandName2Code(const std::string& param) {
    if (param == "set") {
        return MC_COMMAND::CMD_SET;
    } else if (param == "add") {
        return MC_COMMAND::CMD_ADD;
    } else if (param == "get") {
        return MC_COMMAND::CMD_GET;
    } else if (param == "delete") {
        return MC_COMMAND::CMD_DELETE;
    } else if (param == "replace") {
        return MC_COMMAND::CMD_REPLACE;
    } else if (param == "append") {
        return MC_COMMAND::CMD_APPEND;
    } else if (param == "prepend") {
        return MC_COMMAND::CMD_PREPEND;
    } else {
        throw "command not found";
    }
    return MC_COMMAND::CMD_UNKNOWN;
}

void McCommand::Deserialize(SocketRBuffer* buffer) {
    std::string cmd = buffer->ReadField(' ');
    command = CommandName2Code(cmd);
    buffer->ReadChar();
    if (cmd == "get") {
        while (1) {
            std::string key = buffer->ReadField(" \r");
            keys.push_back(key);
            char sep = buffer->ReadChar();
            if (sep == '\r') {
                break;
            }
        }
        buffer->ReadCharCheck('\n');
    } else if (cmd == "delete") {
        std::string key = buffer->ReadField(' ');
        keys.push_back(key);
        buffer->ReadChar();
        buffer->ReadCharCheck('\r');
        buffer->ReadCharCheck('\n');
    } else if (cmd == "set" || cmd == "add" || cmd == "replace") {
        std::string key = buffer->ReadField(' ');
        keys.push_back(key);
        buffer->ReadChar();

        flags = (int)(buffer->ReadUint32());
        buffer->ReadChar();

        exp_time = (time_t)(buffer->ReadUint32());
        buffer->ReadChar();

        int32_t n = buffer->ReadUint32();
        buffer->ReadCharCheck('\r');
        buffer->ReadCharCheck('\n');
        data = buffer->ReadBytes(n);
        buffer->ReadCharCheck('\r');
        buffer->ReadCharCheck('\n');
    } else if (cmd == "append" || cmd == "prepend") {
        std::string key = buffer->ReadField(' ');
        keys.push_back(key);
        buffer->ReadChar();

        int32_t n = buffer->ReadUint32();
        buffer->ReadCharCheck('\r');
        buffer->ReadCharCheck('\n');
        data = buffer->ReadBytes(n);
        for (int32_t i = 0; i < n; ++i) {
        }
        buffer->ReadCharCheck('\r');
        buffer->ReadCharCheck('\n');
    }
}

std::string ResultCode2String(MC_RESULT_CODE code) {
    switch (code) {
        case R_STORED:
            return "STORED\r\n";
            break;
        case R_NOT_STORED:
            return "NOT_STORED\r\n";
            break;
        case R_EXISTS:
            return "EXISTS\r\n";
            break;
        case R_NOT_FOUND:
            return "NOT_FOUND\r\n";
            break;
        case R_DELETED:
            return "DELETED\r\n";
            break;
        default:
            return "";
    }
}

void McValue::Serialize(SocketWBuffer* buffer) const {
    buffer->WriteField(key_.c_str(), ' ');
    
    buffer->WriteUint32(flags_);
    buffer->WriteChar(' ');
        
    size_t bytes = data_.size();
    buffer->WriteUint32(bytes);
    buffer->WriteChar('\r');
    buffer->WriteChar('\n');    
    buffer->WriteBytes(data_);
    buffer->WriteChar('\r');
    buffer->WriteChar('\n'); 
}

void McResult::Serialize(SocketWBuffer* buffer) const {
    if (type_ == RT_ERROR) {
        buffer->WriteField("ERROR", ' ');
        buffer->WriteField(error_message_);
        buffer->WriteChar('\r');
        buffer->WriteChar('\n');
        buffer->Flush();
    } else if (type_ == RT_CODE) {
        std::string code_string = ResultCode2String(code_);
        buffer->WriteField(code_string);
        buffer->Flush();
    } else if (type_ == RT_VALUE) {
        for (McValue mcv : values_) {
            buffer->WriteField("VALUE", ' ');
            mcv.Serialize(buffer);
        }
        buffer->WriteField("END\r\n");
        buffer->Flush();            
    } else {
        throw "unknown McResult data type";
    }
}

void FormatValue(std::vector<char> &data, int flags) {
    char * char_flags = (char*) &flags;
    data.resize(data.size() + sizeof(flags));
    std::copy(char_flags, char_flags + sizeof(flags), data.end() - sizeof(flags));
}

void ParseValue(std::vector<char> &value, int &flags) {
    char *data = value.data();
    flags = *(int*)(data + (value.size() - sizeof(flags)));
    value.resize(value.size() - sizeof(flags));
}

McResult ProcessCommand(const McCommand& command, Cache<std::string, std::vector<char>, int32_t> &cache) {
    std::string serr = "unknown command";
    bool found = true;
    std::vector<char> value = command.data;
    int flags;
    std::vector<McValue> mcv;
    switch(command.command) {
        case MC_COMMAND::CMD_SET:
            FormatValue(value, command.flags);
            if (!cache.set(command.keys[0], value, command.exp_time)) {
                return McResult(MC_RESULT_CODE::R_NOT_STORED);
            }
            return McResult(MC_RESULT_CODE::R_STORED);
            break;
        case MC_COMMAND::CMD_GET:
            for (std::string key : command.keys) {
                if (cache.get(key, value)) {
                    ParseValue(value, flags);
                    mcv.push_back(McValue(key, flags, value));   
                }
            }
            if (!found) {
                return McResult(MC_RESULT_CODE::R_NOT_FOUND);
            } 
            return McResult(mcv);
            break;
        case MC_COMMAND::CMD_DELETE:
            if (!cache.remove(command.keys[0])) {
                return McResult(MC_RESULT_CODE::R_NOT_FOUND);
            }
            return McResult(MC_RESULT_CODE::R_DELETED);
            break;
        case MC_COMMAND::CMD_ADD:
            FormatValue(value, command.flags);
            if (!cache.add(command.keys[0], value, command.exp_time)) {
                return McResult(MC_RESULT_CODE::R_NOT_STORED);
            }
            return McResult(MC_RESULT_CODE::R_STORED);
            break;
        case MC_COMMAND::CMD_REPLACE:
            FormatValue(value, command.flags);
            if (!cache.replace(command.keys[0], value, command.exp_time)) {
                return McResult(MC_RESULT_CODE::R_NOT_STORED);
            }
            return McResult(MC_RESULT_CODE::R_STORED);
            break;
        case MC_COMMAND::CMD_APPEND:
            if (!cache.append(command.keys[0], value)) {
                return McResult(MC_RESULT_CODE::R_NOT_STORED);
            }
            return McResult(MC_RESULT_CODE::R_STORED);
            break;
        case MC_COMMAND::CMD_PREPEND:
            if (!cache.prepend(command.keys[0], value)) {
                return McResult(MC_RESULT_CODE::R_NOT_STORED);
            }
            return McResult(MC_RESULT_CODE::R_STORED);
            break;
        default:
            return McResult(serr);
            break;
    }
    return McResult(serr);
}
