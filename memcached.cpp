#include <atomic>
#include <errno.h>
#include <fcntl.h>
#include <mutex>
#include <netdb.h>
#include <netinet/in.h>
#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <vector>
#include <unistd.h>

#include "cache.h"
#include "protocol.h"

#define BACKLOG 10 
#define BUFFER_SIZE 4096
#define CACHE_SIZE 4096

std::vector<std::thread> threads;
volatile int thread_count = 0;
std::mutex mtx;

class McServer {
private:
    Cache<std::string, std::vector<char>, int32_t> cache_;
    struct addrinfo *servinfo;         
    int sockfd_ = -1;

public:
    explicit McServer(const char * port) 
            : cache_(Cache<std::string, std::vector<char>, int32_t>(CACHE_SIZE)) {
        int status;
        struct addrinfo hints;
        
        memset(&hints, 0, sizeof hints); 
        hints.ai_family = AF_INET;       
        hints.ai_socktype = SOCK_STREAM; 
        hints.ai_flags = AI_PASSIVE;     
        
        if ((status = getaddrinfo(NULL, port, &hints, &servinfo)) != 0) {
            fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
            throw ("getaddrinfo error: " + std::string(gai_strerror(status)));
        }   
        
        sockfd_ = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
        if (sockfd_ == -1) {
            fprintf(stderr, "sockfd error: %s\n", strerror(errno));
            throw("sockfd error: " + std::string(strerror(errno)));
        }
    } 

    void Run() {
        if (bind(sockfd_, servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
            fprintf(stderr, "bind error: %s\n", strerror(errno));       
            throw("bind error: " + std::string(strerror(errno)));
        }
        if (listen(sockfd_, BACKLOG) == -1) {
            fprintf(stderr, "listen error: %s\n", strerror(errno));
            throw("listen error: " + std::string(strerror(errno)));
        }
        try {
            AcceptConnection();
        } catch(...) {
            return;
        }
    }

    void AcceptConnection() {
        int timeout;
        struct pollfd fds[1];
        memset(fds, 0 , sizeof(fds));
        fds[0].fd = sockfd_;
        fds[0].events = POLLIN;
        timeout = 1000*60*10;
        for(;;) {
            int rc = poll(fds, 1, timeout);
            if (rc < 0) {
                fprintf(stderr, "poll() failed");
                throw("poll() failed");
                return;
            }
            if (rc == 0) {
                fprintf(stderr, "poll() timed out\n");
                throw("poll() timed out\n");
                return;
            }
            int connfd = accept(sockfd_, NULL, NULL);
            if (connfd < 0) {
                if (errno != EWOULDBLOCK) {
                    fprintf(stderr, "accept() failed\n");
                    throw("accept() failed\n");
                }
                break;
            }
            threads.push_back(std::thread(&McServer::ProcessConnection, this, connfd));
        }
    }

    void ProcessConnection(int connfd) {
        if (connfd == -1) {
           fprintf(stderr, "accept error: %s\n", strerror(errno));
           throw("bad file descriptor");
        }
        while(1) {
            try {
                McCommand mc_command;
                SocketRBuffer rbuffer(1, connfd);
                mc_command.Deserialize(&rbuffer);
                mtx.lock();
                McResult mc_result = ProcessCommand(mc_command, cache_);
                mtx.unlock();
                SocketWBuffer wbuffer(BUFFER_SIZE, connfd);
                mc_result.Serialize(&wbuffer);
            } catch(std::exception &e) {
                fprintf(stderr, "catch %s\n", e.what()); 
                break;
            }
        }
    }

    ~McServer() {
        freeaddrinfo(servinfo); 
    }
}; 

int main(int argc, char *argv[]) {
    const char * port = argv[1];
    try {
        McServer mc_server(port);
        mc_server.Run();
    } catch(std::exception &e) {
        fprintf(stderr, "server exception %s\n", e.what());
    } 
    for(size_t i = 0; i < threads.size(); ++i) {
        if (threads[i].joinable()) { 
            threads[i].join();
        }
    }
    return 0;
}
