#include <stdexcept>

#include <iterator>
#include <iostream>
#include "buffer.h"

char RBuffer::ReadChar() {
    if (pos_ == end_) {
        try {
            ReadMore();
        } catch(std::exception &e) {
            throw std::runtime_error("Failed to read char in RBuffer.ReadChar: " + (std::string)(e.what()));
        }
    }
    return buffer_[pos_++];
}

void RBuffer::ReadCharCheck(char check) {
    if (ReadChar() != check) {
        throw std::runtime_error("Invalid character in input " + std::to_string(buffer_[pos_]) + " should be " + std::to_string(check));
    }
}

uint32_t RBuffer::ReadUint32() {
    uint32_t value = 0;
    char ch;
    if (!isdigit(ch = ReadChar())) {
        throw std::runtime_error("Failed to read digit");
    } else {
        int digit = ch - '0';
        value *= 10;
        value += digit;
    }
    while (isdigit(ch = ReadChar())) {
        int digit = ch - '0';
        value *= 10;
        value += digit;
    }
    --pos_;

    return value;
}

std::string RBuffer::ReadField(char sep) {
    std::string field;
    char ch;
    while ((ch = ReadChar()) != sep) {
        field.push_back(ch);
    }
    --pos_;
    return field;
}

std::string RBuffer::ReadField(std::string sep) {
    std::string field;
    char ch;
    bool flag = true;
    while (flag) {
        ch = ReadChar();
        for (char sep_char : sep) {
            if (ch == sep_char) {
                flag = false;
                break;
            }
        }
        if (flag) {
            field.push_back(ch);
        }
    }
    --pos_;
    return field;
}

std::vector<char> RBuffer::ReadBytes(size_t bytes_num) {
    std::vector<char> bytes(bytes_num);
    for (size_t i = 0; i < bytes_num; ++i) {
        bytes[i] = ReadChar();
    }

    return bytes;
}

void WBuffer::WriteChar(char ch) {
    buffer_[pos_++] = ch;
    if (pos_ == buffer_.size()) {
        Flush();
    }
}

void WBuffer::WriteUint32(uint32_t v) {
    char buf[32];
    int chw;
    snprintf(buf, 32, "%u%n", v, &chw);
    for (int i = 0; i < chw; ++i) {
        WriteChar(buf[i]);
    }
}

void WBuffer::WriteField(std::string field) {
    for (size_t i = 0; i < field.size(); ++i) {
        WriteChar(field[i]);
    }
}

void WBuffer::WriteField(std::string field, char sep) {
    WriteField(field);
    WriteChar(sep);
}

void WBuffer::WriteBytes(const std::vector<char>& buffer) {
    for (char c : buffer) {
        WriteChar(c);
    }
}

SocketRBuffer::SocketRBuffer(size_t size, int fd)
        : RBuffer(size)
        , fd_(fd)
        , size_(size) {
    ReadMore();
}

void SocketRBuffer::ReadMore() {
    pos_ = 0;
    if (fd_ == -1) {
        throw std::runtime_error("Bad file descriptor");
    }
    size_t bytes_to_read = size_;
    buffer_.resize(size_);
    unsigned char buf[bytes_to_read];
    int total_bytes_read = 0;
    int bytes_read;
    do {
        bytes_read = read(fd_, reinterpret_cast<void*>(&buf), bytes_to_read);
        if (bytes_read == -1) {
            throw std::runtime_error("Failed to read from socket in SocketRBuffer.ReadMore: unknown error");
        }
        if (bytes_read == 0 && total_bytes_read == 0) {
            throw std::runtime_error("Failed to read from socket in SocketRBuffer.ReadMore: eof");
        }
        std::copy(buf, buf + bytes_read, buffer_.begin() + total_bytes_read);
        total_bytes_read += bytes_read;
        bytes_to_read -= bytes_read;     
    } while (bytes_to_read > 0 && bytes_read > 0);
    
    end_ = total_bytes_read;
}

void SocketWBuffer::Flush() {
    int bytes_to_write = pos_;
    int bytes_written;
    char * data = buffer_.data();
    do {
        bytes_written = write(fd_, reinterpret_cast<void*>(data), bytes_to_write);
        if (bytes_written == -1) {  
            throw std::runtime_error("Failed to write to socket in SocketWBuffer.Flush");
        }
        data += bytes_written;
        bytes_to_write -= bytes_written;       
    } while (bytes_to_write > 0);
    pos_ = 0;
}

